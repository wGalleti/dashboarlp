package sqls

var (
	SqlFilial = `
		select
			ef.id_filial as id,
			ef.nome_fantasia || ' ' || upper(m.nome) as nome
		from
			empresa_filial ef
			join municipio m on ef.cod_municipio = m.cod_municipio
		where
			ef.id_filial = ?
	`
	SqlFiliais = `
		select
			ef.id_filial as id,
			ef.nome_fantasia || ' ' || upper(m.nome) as nome
		from
			empresa_filial ef
			join municipio m on ef.cod_municipio = m.cod_municipio
		order by
			ef.id_filial
	`

	SqlMeta = `
		select
			vm.id,
			vm.descricao as nome
		from
			venda_meta vm
		where
			vm.id = ?
		order by
			1
	`

	SqlMetas = `
		select
			vm.id,
			vm.descricao as nome
		from
			venda_meta vm
		where
			vm.filial_id = ?
		order by
			1
	`

	SqlGraficoMensalFilial = `
		select
			substring(vpm.descricao from 1 for iif(position('-', vpm.descricao) > 1, position('-', vpm.descricao) -1, char_length(vpm.descricao))) as ordem,
			substring(vpm.descricao from position('-', vpm.descricao) + 1) as descricao,
			sum(vpm.previsto) as previsto,
			sum(vpm.realizado) as realizado
		from
			v_pesquisa_meta vpm
		where
			vpm.filial_id = ?
			and extract(year from inicio) = extract(year from current_date)
		group by
			1,
			2
		order by
			1
	`

	SqlMetaAtual = `
		select
			sum(vpm.previsto) as previsto,
			sum(vpm.realizado) as realizado
		from
			v_pesquisa_meta vpm
		where
			vpm.id = ?
			and vpm.filial_id = ?
	`
	SqlDetalheVendedor = `
		select
			vpm.vendedor_id as pk,
			vpm.vendedor as nome,
			vpm.previsto,
			vpm.realizado,
			vpm.vendas,
			cast(sum(vpm.realizado) over() as numeric(15, 4)) as total,
			vpm.realizado / cast(sum(vpm.realizado) over() as numeric(15, 4)) * 100 as referencia
		from
			v_pesquisa_meta vpm
		where
			vpm.id = ?
			and vpm.filial_id = ?
		order by
			7 desc
	`

	SqlSubDetalheVendedor = `
		select
			c.razao_social as cliente,
			v.venda,
			v.data,
			fp.nome as pagamento,
			gp.nome || ' - ' || sp.nome as categoria,
			p.nome || iif(tt.id is null, '', ' - TAMANHO ' || tt.descricao) as produto,
			v.quantidade,
			v.desconto,
			v.unitario,
			v.total
		from
			venda_meta vm
			join venda_meta_vendedor vmv on vm.id = vmv.venda_meta_id
			join (
				select
					v.id_filial as filial_id,
					v.vendedor_id as vendedor_id,
					v.cliente_id as cliente_id,
					v.id_forma as pagamento_id,
					v.num_pedido as venda,
					v.data_fechamento as data,
					vi.cod_produto as produto_id,
					vi.produto_tamanho_id,
					sum(vi.quantidade) as quantidade,
					sum(coalesce(vi.valor_desconto, 0)) as desconto,
					sum(vi.preco_venda) as unitario,
					sum(vi.quantidade * vi.preco_venda - coalesce(vi.valor_desconto, 0)) as total
				from
					venda v
					join venda_item vi
						on v.num_pedido = vi.num_pedido
						and v.tipo_pedido = vi.tipo_pedido
				where
					coalesce(v.cancelado, 0) = 0
					and coalesce(v.contabilizado, 0) = 1
					and exists (
						select
							1
						from
							venda_fechamento vf
						where
							vf.num_pedido = v.num_pedido
							and vf.tipo_pedido = v.tipo_pedido
					)
				group by
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8
			) v
				on vm.filial_id = v.filial_id
				and v.data between vm.inicio and vm.fim
				and vmv.vendedor_id = v.vendedor_id
			join forma_pagamento fp on v.pagamento_id = fp.id_forma
			join cliente c on v.cliente_id = c.id_cliente
			join produto p on v.produto_id = p.cod_produto
			join subgrupo_produto sp
				on p.cod_grupo = sp.cod_grupo
				and p.cod_sub_grupo = sp.cod_sub_grupo
			join grupo_produto gp on sp.cod_grupo = gp.cod_grupo
			left join produto_tamanho pp on v.produto_tamanho_id = pp.id
			left join tabela_tamanho tt on pp.id_tamanho = tt.id
		where
			vm.id = ?
			and vm.filial_id = ?
			and v.vendedor_id = ?
	`

	SqlMetaCaixa = `
		select
			coalesce(v.caixa, 'SEM CAIXA') as caixa,
			count(distinct v.venda) as vendas,
			cast(sum(v.total) as numeric(15, 2)) as total
		from
			venda_meta vm
			join venda_meta_vendedor vmv on vm.id = vmv.venda_meta_id
			join (
				select
					v.id_filial as filial_id,
					v.vendedor_id as vendedor_id,
					v.cliente_id as cliente_id,
					v.id_forma as pagamento_id,
					v.num_pedido as venda,
					v.data_fechamento as data,
					vi.cod_produto as produto_id,
					vi.produto_tamanho_id,
					upper(u.login) as caixa,
					sum(vi.quantidade) as quantidade,
					sum(coalesce(vi.valor_desconto, 0)) as desconto,
					sum(vi.preco_venda) as unitario,
					sum(vi.quantidade * vi.preco_venda - coalesce(vi.valor_desconto, 0)) as total
				from
					venda v
					join venda_item vi
						on v.num_pedido = vi.num_pedido
						and v.tipo_pedido = vi.tipo_pedido
					left join pdv_caixa pc on v.id_pdv_caixa_ref = pc.id_referencia
					left join usuario u on pc.id_caixa = u.id_usuario
				where
					coalesce(v.cancelado, 0) = 0
					and coalesce(v.contabilizado, 0) = 1
					and exists (
						select
							1
						from
							venda_fechamento vf
						where
							vf.num_pedido = v.num_pedido
							and vf.tipo_pedido = v.tipo_pedido
					)
				group by
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9
			) v
				on vm.filial_id = v.filial_id
				and v.data between vm.inicio and vm.fim
				and vmv.vendedor_id = v.vendedor_id
			join forma_pagamento fp on v.pagamento_id = fp.id_forma
			join cliente c on v.cliente_id = c.id_cliente
			join produto p on v.produto_id = p.cod_produto
			join subgrupo_produto sp
				on p.cod_grupo = sp.cod_grupo
				and p.cod_sub_grupo = sp.cod_sub_grupo
			join grupo_produto gp on sp.cod_grupo = gp.cod_grupo
			left join produto_tamanho pp on v.produto_tamanho_id = pp.id
			left join tabela_tamanho tt on pp.id_tamanho = tt.id
		where
			vm.id = ?
			and vm.filial_id = ?
		group by
			1
	`

	SqlCaixaDiario = `
		select
			v.data_fechamento as data,
			upper(coalesce(u.login, 'SEM CAIXA')) as caixa,
			count(distinct v.num_pedido) as vendas,
			cast(sum(vi.quantidade * vi.preco_venda - coalesce(vi.valor_desconto, 0)) as numeric(15, 2)) as total
		from
			venda v
			join venda_item vi
				on v.num_pedido = vi.num_pedido
				and v.tipo_pedido = vi.tipo_pedido
			left join pdv_caixa pc on v.id_pdv_caixa_ref = pc.id_referencia
			left join usuario u on pc.id_caixa = u.id_usuario
		where
			coalesce(v.id_filial, 0) = ?
			and v.data_fechamento >= dateadd(day, -5, current_date)
			and coalesce(v.cancelado, 0) = 0
			and coalesce(v.contabilizado, 0) = 1
			and exists (
				select
					1
				from
					venda_fechamento vf
				where
					vf.num_pedido = v.num_pedido
					and vf.tipo_pedido = v.tipo_pedido
			)
		group by
			1,
			2
		order by
			1,
			2
	`
)
