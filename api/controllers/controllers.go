package controllers

import (
	"github.com/gofiber/fiber/v2"
	"hfzdashboard/database"
	"hfzdashboard/models"
	"hfzdashboard/sqls"
)

type Controllers interface {
	FilialCaixa(c *fiber.Ctx) error
	MetaCaixa(c *fiber.Ctx) error
	SubDetalheVendedor(c *fiber.Ctx) error
	DetalheVendedor(c *fiber.Ctx) error
	MetaAutal(c *fiber.Ctx) error
	MetaFilialMensal(c *fiber.Ctx) error
	ListarMetas(c *fiber.Ctx) error
	GetMeta(c *fiber.Ctx) error
	ListarFilial(c *fiber.Ctx) error
	GetFilial(c *fiber.Ctx) error
}

type controller struct {
	db database.Connection
}

func NewController(db database.Connection) Controllers {
	return &controller{db: db}
}

func (ct *controller) FilialCaixa(c *fiber.Ctx) error {
	filial, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}

	var dados []models.FilialCaixa
	err = ct.db.DB().Select(&dados, sqls.SqlCaixaDiario, filial)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) MetaCaixa(c *fiber.Ctx) error {
	meta, err := c.ParamsInt("meta")
	if err != nil {
		c.Status(401)
	}

	filial, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}

	var dados []models.MetaCaixa

	err = ct.db.DB().Select(&dados, sqls.SqlMetaCaixa, meta, filial)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) SubDetalheVendedor(c *fiber.Ctx) error {
	meta, err := c.ParamsInt("meta")
	if err != nil {
		c.Status(401)
	}

	filial, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}

	vendedor, err := c.ParamsInt("vendedor")
	if err != nil {
		c.Status(401)
	}
	var dados []models.VendedorSubDetalhe

	err = ct.db.DB().Select(&dados, sqls.SqlSubDetalheVendedor, meta, filial, vendedor)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) DetalheVendedor(c *fiber.Ctx) error {
	meta, err := c.ParamsInt("meta")
	if err != nil {
		c.Status(401)
	}

	filial, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}
	var dados []models.VendedorDetalhe

	err = ct.db.DB().Select(&dados, sqls.SqlDetalheVendedor, meta, filial)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) MetaAutal(c *fiber.Ctx) error {
	meta, err := c.ParamsInt("meta")
	if err != nil {
		c.Status(401)
	}

	filial, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}
	var dados models.MetaAtual

	err = ct.db.DB().Get(&dados, sqls.SqlMetaAtual, meta, filial)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) MetaFilialMensal(c *fiber.Ctx) error {
	param, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}
	var dados []models.GraficoMetaFilial

	err = ct.db.DB().Select(&dados, sqls.SqlGraficoMensalFilial, param)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(dados)
}

func (ct *controller) ListarMetas(c *fiber.Ctx) error {
	param, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}

	var metas []models.Meta

	err = ct.db.DB().Select(&metas, sqls.SqlMetas, param)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(metas)
}

func (ct *controller) GetMeta(c *fiber.Ctx) error {
	param, err := c.ParamsInt("meta")
	if err != nil {
		c.Status(401)
	}
	var meta models.Meta

	err = ct.db.DB().Get(&meta, sqls.SqlMeta, param)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(meta)
}

func (ct *controller) ListarFilial(c *fiber.Ctx) error {

	var filiais []models.Filial

	err := ct.db.DB().Select(&filiais, sqls.SqlFiliais)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(filiais)
}

func (ct *controller) GetFilial(c *fiber.Ctx) error {
	param, err := c.ParamsInt("filial")
	if err != nil {
		c.Status(401)
	}
	var filial models.Filial

	err = ct.db.DB().Get(&filial, sqls.SqlFilial, param)

	if err != nil {
		c.Status(401)
	}

	return c.Status(200).JSON(filial)
}
