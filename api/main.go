package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/joho/godotenv"
	"hfzdashboard/controllers"
	"hfzdashboard/database"
	"log"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Panicln(err)
	}
}

func main() {
	conn := database.NewConnection()
	defer conn.Disconnect()

	app := fiber.New()
	app.Use(logger.New())
	app.Use(cors.New())
	c := controllers.NewController(conn)

	app.Static("/", "./html")
	app.Static("/media", "./media")
	app.Get("/filiais", c.ListarFilial)
	app.Get("/filiais/:filial", c.GetFilial)
	app.Get("/metas/:filial", c.ListarMetas)
	app.Get("/metas/:meta", c.GetMeta)
	app.Get("/metas/mensal/:filial", c.MetaFilialMensal)
	app.Get("/metas/atual/:meta/:filial", c.MetaAutal)
	app.Get("/vendedores/:meta/:filial", c.DetalheVendedor)
	app.Get("/subdetalhe/:meta/:filial/:vendedor", c.SubDetalheVendedor)
	app.Get("/caixa/:meta/:filial", c.MetaCaixa)
	app.Get("/caixa/:filial", c.FilialCaixa)

	log.Fatal(app.Listen(":9000"))
}
