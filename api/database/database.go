package database

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/nakagami/firebirdsql"
	"log"
	"os"
)

func getDatabaseUrl() string {
	return os.Getenv("DATABASE_URL")
}

type Connection interface {
	DB() *sqlx.DB
	Disconnect()
}

type connection struct {
	db *sqlx.DB
}

func NewConnection() Connection {
	var c connection
	var err error
	url := getDatabaseUrl()
	c.db, err = sqlx.Connect("firebirdsql", url)

	if err != nil {
		log.Fatalln(err)
	}

	err = c.db.Ping()

	if err != nil {
		log.Fatalln(err)
	}

	return &c
}

func (c *connection) DB() *sqlx.DB {
	return c.db
}

func (c *connection) Disconnect() {
	c.db.Close()
}
