package models

import (
	"fmt"
	"time"

	"github.com/guregu/null"
)

type JSONTime time.Time

func (t JSONTime) MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("2006-02-01"))
	return []byte(stamp), nil
}

type Filial struct {
	Id   null.Int    `json:"id" db:"ID"`
	Nome null.String `json:"nome" db:"NOME"`
}

type Meta struct {
	Id   null.Int    `json:"id" db:"ID"`
	Nome null.String `json:"nome" db:"NOME"`
}

type GraficoMetaFilial struct {
	Ordem     null.String `json:"ordem" db:"ORDEM"`
	Descricao null.String `json:"descricao" db:"DESCRICAO"`
	Previsto  null.Float  `json:"previsto" db:"PREVISTO"`
	Realizado null.Float  `json:"realizado" db:"REALIZADO"`
}

type MetaAtual struct {
	Previsto  null.Float `json:"previsto" db:"PREVISTO"`
	Realizado null.Float `json:"realizado" db:"REALIZADO"`
}

type VendedorDetalhe struct {
	Pk         null.Int    `json:"pk" db:"PK"`
	Nome       null.String `json:"nome" db:"NOME"`
	Previsto   null.Float  `json:"previsto" db:"PREVISTO"`
	Realizado  null.Float  `json:"realizado" db:"REALIZADO"`
	Vendas     null.Float  `json:"vendas" db:"VENDAS"`
	Total      null.Float  `json:"total" db:"TOTAL"`
	Referencia null.Float  `json:"referencia" db:"REFERENCIA"`
}

type VendedorSubDetalhe struct {
	Cliente    null.String `json:"cliente" db:"CLIENTE"`
	Venda      null.Int    `json:"venda" db:"VENDA"`
	Data       JSONTime    `json:"data" db:"DATA"`
	Pagamento  null.String `json:"pagamento" db:"PAGAMENTO"`
	Categoria  null.String `json:"categoria" db:"CATEGORIA"`
	Produto    null.String `json:"produto" db:"PRODUTO"`
	Quantidade null.Float  `json:"quantidade" db:"QUANTIDADE"`
	Desconto   null.Float  `json:"desconto" db:"DESCONTO"`
	Unitario   null.Float  `json:"unitario" db:"UNITARIO"`
	Total      null.Float  `json:"total" db:"TOTAL"`
}

type MetaCaixa struct {
	Caixa  null.String `json:"caixa" db:"CAIXA"`
	Vendas null.Float  `json:"vendas" db:"VENDAS"`
	Total  null.Float  `json:"total" db:"TOTAL"`
}

type FilialCaixa struct {
	Data   JSONTime    `json:"data" db:"DATA"`
	Caixa  null.String `json:"caixa" db:"CAIXA"`
	Vendas null.Float  `json:"vendas" db:"VENDAS"`
	Total  null.Float  `json:"total" db:"TOTAL"`
}
